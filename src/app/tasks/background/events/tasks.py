from logging import getLogger
from typing import Any

from email_validator import validate_email, EmailNotValidError
from orjson import orjson

from app.apm import apm_transaction
from app.database import session_scope
from app.integrations.email import EmailHttpClient, EmailHttpClientError
from app.models import Notification, StatusEnum, DeliveryMethodEnum

logger = getLogger(__name__)


class BaseTaskError(Exception):
    pass


class NotificationNotFoundError(BaseTaskError):
    pass


class NotificationBadStatusError(BaseTaskError):
    pass


class NotificationBadTypeError(BaseTaskError):
    pass


@apm_transaction(
    name="Send email task",
    transaction_type="background",
    labels_builder=lambda f, msg, _: msg,
)
async def send_email_task(
    message: dict[str, Any], email_client: EmailHttpClient
) -> None:
    """
    Callback method called when message from RabbitMQ received.
    Args:
        message: dictionary holding RabbitMQ message.
                 message["body"] holding payload.

        email_client: instance of object responsible for notification delivery to the user.

        payload example:
        {"data":"77b2eddb-a0b7-47e1-86dc-0c51bae48f42"}
    """
    payload = orjson.loads(message["body"])
    notification_id = payload.get("data")

    logger.info("Starting work with notification with id: %s", notification_id)

    async with session_scope() as db_session:
        notification = await Notification.get_by_id(db_session, notification_id)

        try:
            validate_notification(notification)
        except (NotificationBadStatusError, NotificationBadTypeError):
            return
        except EmailNotValidError:
            notification.status = StatusEnum.CANCELED
            await db_session.commit()
            return

        notification.status = StatusEnum.IN_PROGRESS
        await db_session.commit()
        await db_session.refresh(notification)

        try:
            await email_client.send(
                email=notification.contact,
                username=notification.username or "",
                subject=notification.template.name,
                message_text=notification.template.data,
            )
        except EmailHttpClientError as err:
            logger.debug(
                "Sending email for notification %s failed: %s", notification.id, err
            )
            notification.status = StatusEnum.CREATED
            await db_session.commit()
            raise

        notification.status = StatusEnum.SENT


def validate_notification(notification: Notification) -> None:
    if notification.status != StatusEnum.CREATED:
        logger.error(
            "Notification already has %s status. No actions required.",
            notification.status,
        )
        raise NotificationBadStatusError

    if notification.delivery_method != DeliveryMethodEnum.EMAIL:
        logger.error("Notification has different than email delivery method.")
        raise NotificationBadTypeError

    try:
        validate_email(notification.contact)
    except EmailNotValidError as err:
        logger.error(
            "Notification has no email address. Contact data: %s, Error: %s",
            notification.contact,
            err,
        )
        raise
