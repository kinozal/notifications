from typing import Optional

from aio_pika import ExchangeType

from app.amqp import ExchangeNameEnum, RoutingKeyEnum, QueueNameEnum
from app.integrations.email import EmailHttpClient
from app.prx_aio_rabbitmq.consumer import Consumer
from app.settings import settings
from app.tasks.background.events import tasks


class Worker:
    def __init__(self, email_client: EmailHttpClient) -> None:
        self.email_client = email_client
        self.consumer: Optional[Consumer] = None

    async def startup(self) -> None:
        await self.email_client.http_transport.startup()

        self.consumer = Consumer(
            url=settings.RMQ.DSN,
            exchange_name=ExchangeNameEnum.default.value,
            exchange_type=ExchangeType.DIRECT,
            routing_key=RoutingKeyEnum.email.value,
            queue_name=QueueNameEnum.email.value,
            exchange_durable=True,
            callback=tasks.send_email_task,
            callback_data=self.email_client,
        )

    async def shutdown(self) -> None:
        await self.email_client.http_transport.shutdown()
        await self.consumer.disconnect()

    async def run(self) -> None:
        await self.consumer.connect()
