from typing import Any

from arq.connections import RedisSettings
from arq.cron import cron, CronJob

from app.amqp import publisher
from app.integrations.auth import AuthHttpClient
from app.prx_aio_rabbitmq.publisher import Publisher
from app.redis import redis_settings
from app.tasks.periodic.events.email import tasks
from app.transports import AiohttpTransport


async def startup(ctx: dict[str, Any]) -> None:
    await publisher.connect()
    ctx["publisher"] = publisher

    auth_client = AuthHttpClient(AiohttpTransport())
    await auth_client.http_transport.startup()
    ctx["auth_client"] = auth_client


async def shutdown(ctx: dict[str, Any]) -> None:
    publisher: Publisher = ctx["publisher"]
    await publisher.disconnect()

    auth_client: AuthHttpClient = ctx["auth_client"]
    await auth_client.http_transport.shutdown()


class EmailWorker:
    redis_settings: RedisSettings = redis_settings
    on_startup = startup
    on_shutdown = shutdown
    cron_jobs: list[CronJob] = [
        cron(
            tasks.send_email_to_long_time_non_visiting_users,
            # day=settings.PERIODIC.NON_VISITING_USERS.TIMEDELTA_DAYS
        ),
    ]
