import asyncio
from datetime import datetime, timedelta
from typing import Any

from app.amqp import enqueue_job, QueueNameEnum
from app.apm import apm_transaction
from app.database import session_scope
from app.integrations.auth import AuthHttpClient
from app.models import Template, Notification
from app.settings import settings


@apm_transaction(
    name="Send email to long time non-visiting users",
    transaction_type="periodic",
)
async def send_email_to_long_time_non_visiting_users(ctx: dict[str, Any]) -> None:
    async with session_scope() as db_session:
        target_template = await Template.get_by_name(
            db_session,
            settings.PERIODIC.NON_VISITING_USERS.TEMPLATE_NAME,
        )

        auth_client: AuthHttpClient = ctx["auth_client"]

        target_datetime = datetime.now() - timedelta(
            days=settings.PERIODIC.NON_VISITING_USERS.TIMEDELTA_DAYS,
        )

        async for users in auth_client.get_long_time_non_visiting_users(
            target_datetime
        ):
            notifications = [
                Notification(
                    contact=user.email,
                    username=user.name,
                    template_id=target_template.id,
                )
                for user in users
            ]
            db_session.add_all(notifications)
            await db_session.flush(notifications)

            tasks = [
                asyncio.create_task(
                    enqueue_job(
                        queue_name=QueueNameEnum.email.value, data=str(notification.id)
                    ),
                )
                for notification in notifications
            ]

            await asyncio.gather(*tasks)
