"""add canceled status

Revision ID: 2aa75436601e
Revises: 3e5add4f96f7
Create Date: 2022-07-27 21:38:27.009878

"""
from alembic import op

# revision identifiers, used by Alembic.
from sqlalchemy import orm

revision = "2aa75436601e"
down_revision = "3e5add4f96f7"
branch_labels = None
depends_on = None


def upgrade() -> None:
    bind = op.get_bind()
    session = orm.Session(bind=bind)
    session.execute("ALTER TYPE statusenum ADD VALUE 'CANCELED'")
    session.commit()


def downgrade() -> None:
    pass
