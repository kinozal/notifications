from fastapi import APIRouter

from app.api.internal import v1

internal_api = APIRouter()

internal_api.include_router(v1.notifications.router, prefix="/v1/notifications")
