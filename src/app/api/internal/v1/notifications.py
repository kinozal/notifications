from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from app.amqp import enqueue_job, QueueNameEnum
from app.api.dependencies.database import get_db
from app.api.internal.v1.schemas import (
    NotificationOutputSchema,
    NotificationInputSchema,
)
from app.api.schemas import ErrorSchema
from app.models import DeliveryMethodEnum
from app.services.notifications import (
    NotificationService,
    get_notification_service,
    TemplateDoesNotExist,
)

router = APIRouter()


@router.post(
    "/send-email",
    responses={
        status.HTTP_404_NOT_FOUND: {"model": ErrorSchema},
    },
    response_model=NotificationOutputSchema,
    description="Generates email notification and queues the task.",
)
async def create_email_notification(
    data: NotificationInputSchema,
    service: NotificationService = Depends(get_notification_service),
    db_session: AsyncSession = Depends(get_db),
):
    try:
        notification = await service.create_notification_by_template_name(
            db_session,
            data.template_name,
            data.contact,
            data.username,
            DeliveryMethodEnum.EMAIL,
        )
    except TemplateDoesNotExist:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Template not found."
        )

    await enqueue_job(
        queue_name=QueueNameEnum.email.value,
        data=str(notification.id),
    )

    return NotificationOutputSchema.from_orm(notification)
