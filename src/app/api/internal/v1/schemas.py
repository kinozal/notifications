from datetime import datetime

from app.models import StatusEnum, DeliveryMethodEnum
from pydantic import BaseModel, UUID4


class NotificationInputSchema(BaseModel):
    contact: str
    username: str
    template_name: str


class NotificationOutputSchema(BaseModel):
    id: UUID4
    created_at: datetime
    updated_at: datetime
    template_id: UUID4
    contact: str
    username: str
    status: StatusEnum
    delivery_method: DeliveryMethodEnum

    class Config:
        orm_mode = True
