import sqlalchemy as sa
from http import HTTPStatus
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi_pagination import LimitOffsetPage
from fastapi_pagination.ext.async_sqlalchemy import paginate
from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.admin.v1.schemas import TemplateOutputSchema, TemplateInputSchema
from app.api.dependencies.database import get_db
from app.models import Template, ObjectDoesNotExistError, ObjectAlreadyExistError

router = APIRouter()


@router.get(
    "/",
    response_model=LimitOffsetPage[TemplateOutputSchema],
    description="Get paginated templates list.",
)
async def get_templates_list(db_session: AsyncSession = Depends(get_db)):
    return await paginate(db_session, sa.select(Template))


@router.get(
    "/{template_id}",
    response_model=TemplateOutputSchema,
    description="Retrieve template.",
)
async def get_template_by_id(
    template_id: UUID4,
    db_session: AsyncSession = Depends(get_db),
):
    try:
        template = await Template.get_by_id(db_session, template_id)
    except ObjectDoesNotExistError:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND, detail="Template not found."
        )

    return TemplateOutputSchema.from_orm(template)


@router.post("/", response_model=TemplateOutputSchema)
async def add_template(
    template: TemplateInputSchema,
    db_session: AsyncSession = Depends(get_db),
):
    try:
        return await Template.create(db_session, name=template.name, data=template.data)
    except ObjectAlreadyExistError:
        raise HTTPException(
            status_code=HTTPStatus.CONFLICT,
            detail=f"Cannot create template. Template {template.name} already exists",
        )


@router.patch("/{template_id}", response_model=TemplateOutputSchema)
async def patch_template(
    template_id: UUID4,
    template: TemplateInputSchema,
    db_session: AsyncSession = Depends(get_db),
):
    try:
        return await Template.edit_template(
            db_session, template_id, **template.dict(exclude_unset=True)
        )
    except ObjectDoesNotExistError:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND, detail="Template not found."
        )


@router.delete("/{template_id}", status_code=HTTPStatus.NO_CONTENT)
async def delete_template(
    template_id: UUID4,
    db_session: AsyncSession = Depends(get_db),
):
    await Template.del_template(db_session, template_id)
