from fastapi import APIRouter, status, HTTPException, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.amqp import QueueNameEnum, enqueue_job
from app.api.admin.v1.schemas import NotificationInputSchema, NotificationOutputSchema
from app.api.dependencies.database import get_db
from app.models import DeliveryMethodEnum
from app.services.notifications import (
    NotificationService,
    get_notification_service,
    TemplateDoesNotExist,
)

router = APIRouter()


@router.post(
    "/send-email",
    response_model=NotificationOutputSchema,
    description="Generates email notification and queues the task.",
)
async def create_email_notification(
    data: NotificationInputSchema,
    service: NotificationService = Depends(get_notification_service),
    db_session: AsyncSession = Depends(get_db),
):
    try:
        notification = await service.create_notification_by_template_id(
            db_session,
            data.template_id,
            data.contact,
            data.username,
            DeliveryMethodEnum.EMAIL,
        )
    except TemplateDoesNotExist:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Template not found."
        )

    await enqueue_job(
        queue_name=QueueNameEnum.email.value,
        data=str(notification.id),
    )

    return NotificationOutputSchema.from_orm(notification)
