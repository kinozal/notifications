import enum
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, UUID4

from app.models import StatusEnum, DeliveryMethodEnum


class NotificationTypeEnum(str, enum.Enum):
    email = "email"
    sms = "sms"
    push = "push"


class NotificationInputSchema(BaseModel):
    contact: str
    username: str
    type: NotificationTypeEnum
    template_id: UUID4


class NotificationOutputSchema(BaseModel):
    id: UUID4
    created_at: datetime
    updated_at: datetime
    template_id: UUID4
    contact: str
    username: str
    status: StatusEnum
    delivery_method: DeliveryMethodEnum

    class Config:
        orm_mode = True


class TemplateInputSchema(BaseModel):
    data: Optional[str]
    name: Optional[str]


class TemplateOutputSchema(BaseModel):
    id: UUID4
    created_at: datetime
    updated_at: datetime
    data: str
    name: str

    class Config:
        orm_mode = True
