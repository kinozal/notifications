from datetime import datetime
from typing import AsyncGenerator, Any

from furl import furl
from pydantic import AnyHttpUrl, ValidationError, BaseModel

from app.integrations.base import AbstractHttpClient
from app.settings import settings
from app.transports import AbstractHttpTransport


class AuthHttpClientError(Exception):
    pass


class UserSchema(BaseModel):
    email: str
    name: str


class UsersListSchema(BaseModel):
    items: list[UserSchema]
    total: int
    limit: int
    offset: int


class AuthHttpClient(AbstractHttpClient):
    base_url: AnyHttpUrl = settings.AUTH_INTEGRATION.BASE_URL
    client_exc: Exception = AuthHttpClientError

    def __init__(self, http_transport: AbstractHttpTransport) -> None:
        self.http_transport: AbstractHttpTransport = http_transport

    async def get_long_time_non_visiting_users(
        self,
        last_login__lte: datetime,
        limit: int = settings.AUTH_INTEGRATION.USERS_PER_PAGE,
    ) -> AsyncGenerator[list[UserSchema], Any]:
        offset = 0

        while True:
            url = furl(self.base_url).add(
                path="/api/internal/v1/users",
                query_params={
                    "last_login__lte": last_login__lte,
                    "limit": limit,
                    "offset": offset,
                },
            )

            response = await self.request(
                method="GET", url=url.url, timeout=settings.AUTH_INTEGRATION.TIMEOUT_SEC
            )

            try:
                users_schema = UsersListSchema(**response)
            except ValidationError as err:
                raise self.client_exc(err) from err

            yield users_schema.items

            offset += limit

            if offset >= users_schema.total:
                return
