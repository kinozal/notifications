from typing import Type

from jinja2 import Template

from app.transports import AbstractHttpTransport
from pydantic import AnyHttpUrl

from app.integrations.base import AbstractHttpClient
from app.settings import settings


class EmailHttpClientError(Exception):
    pass


class EmailHttpClient(AbstractHttpClient):
    base_url: AnyHttpUrl = settings.EMAIL.BASE_URL
    client_exc: Type[Exception] = EmailHttpClientError
    api_key: str = settings.EMAIL.API_KEY
    address_from: str = settings.EMAIL.ADDRESS_FROM

    def __init__(self, http_transport: AbstractHttpTransport) -> None:
        self.http_transport: AbstractHttpTransport = http_transport

    async def send(
        self, email: str, username: str, subject: str, message_text: str
    ) -> None:
        """
        Sends email via MailGun service
        Args:
            email: user"s email address
            username: Name and Surname (ie "Ivan Ivanov")
            subject: email"s subject
            message_text: jijna2 template of email which is going to be sent
        """
        jinja_template = Template(message_text)

        await self.request(
            method="POST",
            url=self.base_url,
            auth={"api": self.api_key},
            data={
                "from": self.address_from,
                "to": [email],
                "subject": subject,
                "text": jinja_template.render(username=username),
            },
        )
