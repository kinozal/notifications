import enum

import orjson
from aio_pika import ExchangeType

from app.context import ctx
from app.prx_aio_rabbitmq.publisher import Publisher
from app.settings import settings


class ExchangeNameEnum(str, enum.Enum):
    default = "notifications.default"


class RoutingKeyEnum(str, enum.Enum):
    email = "notifications.send-email"


class QueueNameEnum(str, enum.Enum):
    email = "notifications.send-email"


publisher = Publisher(
    url=settings.RMQ.DSN,
    exchange_name=ExchangeNameEnum.default.value,
    exchange_type=ExchangeType.DIRECT,
    exchange_durable=True,
)


async def enqueue_job(queue_name: str, data: str) -> None:
    message = {
        "data": data,
        **ctx.get().dict(),
    }
    str_message = orjson.dumps(message).decode()

    await publisher.publish(
        routing_key=queue_name,
        message_body=str_message,
        persistent=True,
    )
