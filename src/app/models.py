import enum
import uuid
from datetime import datetime

import sqlalchemy as sa
from pydantic import UUID4
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import declarative_mixin, selectinload

from app.database import Base


class ObjectDoesNotExistError(Exception):
    """Raise it if object does not exist in database."""


class ObjectAlreadyExistError(Exception):
    """Raise it if object already exist in database."""


class StatusEnum(enum.Enum):
    CREATED = "CREATED"
    IN_PROGRESS = "IN_PROGRESS"
    SENT = "SENT"
    CANCELED = "CANCELED"


class DeliveryMethodEnum(enum.Enum):
    EMAIL = "EMAIL"
    PUSH = "PUSH"
    SMS = "SMS"


@declarative_mixin
class TimestampMixin:
    created_at = sa.Column(
        sa.DateTime(timezone=False), default=lambda: datetime.utcnow()
    )
    updated_at = sa.Column(
        sa.DateTime(timezone=False),
        default=lambda: datetime.utcnow(),
        onupdate=datetime.utcnow,
    )


class Template(Base, TimestampMixin):
    __tablename__ = "templates"

    id = sa.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    data = sa.Column(sa.TEXT, nullable=False)
    name = sa.Column(sa.String(length=64), nullable=False, unique=True, index=True)

    @classmethod
    async def _get_obj(cls, db_session, stmt: sa.sql.Select) -> "Template":
        result = await db_session.execute(stmt)
        obj = result.scalar()

        if not obj:
            raise ObjectDoesNotExistError

        return obj

    @classmethod
    async def get_by_id(
        cls, db_session: AsyncSession, template_id: UUID4
    ) -> "Template":
        stmt = sa.select(Template).where(Template.id == template_id)
        return await cls._get_obj(db_session, stmt)

    @classmethod
    async def get_by_name(cls, db_session: AsyncSession, name: str) -> "Template":
        stmt = sa.select(Template).where(Template.name == name)
        return await cls._get_obj(db_session, stmt)

    @classmethod
    async def create(cls, db_session: AsyncSession, name: str, data: str) -> "Template":
        template = Template(name=name, data=data)
        db_session.add(template)
        try:
            await db_session.commit()
        except sa.exc.IntegrityError:
            raise ObjectAlreadyExistError
        await db_session.refresh(template)

        return template

    @classmethod
    async def del_template(cls, db_session: AsyncSession, template_id: UUID4):
        stmt = sa.delete(Template).where(Template.id == template_id)
        await db_session.execute(stmt)

    @classmethod
    async def edit_template(
        cls, db_session: AsyncSession, template_id: UUID4, **template
    ):
        db_template = await Template.get_by_id(db_session, template_id)
        for key, value in template.items():
            setattr(db_template, key, value)

        return db_template


class Notification(Base, TimestampMixin):
    __tablename__ = "notifications"

    id = sa.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    template_id = sa.Column(
        UUID(as_uuid=True), sa.ForeignKey("templates.id", ondelete="SET NULL")
    )
    template = sa.orm.relationship("Template")
    contact = sa.Column(sa.String(length=64), nullable=False, index=True)
    username = sa.Column(sa.String(length=256))
    status = sa.Column(sa.Enum(StatusEnum), default=StatusEnum.CREATED.value)
    delivery_method = sa.Column(sa.Enum(DeliveryMethodEnum), nullable=False)

    @classmethod
    async def get_by_id(
        cls, session: AsyncSession, notification_id: UUID4
    ) -> "Notification":
        stmt = (
            sa.select(Notification)
            .where(Notification.id == notification_id)
            .options(selectinload(Notification.template))
        )
        result = await session.execute(stmt)
        obj = result.scalar()

        if not obj:
            raise ObjectDoesNotExistError

        return obj
