from logging import getLogger

from pydantic import UUID4
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import (
    Template,
    Notification,
    ObjectDoesNotExistError,
    DeliveryMethodEnum,
)

logger = getLogger(__name__)


class BaseNotificationServiceError(Exception):
    """Base notification service error."""


class TemplateDoesNotExist(BaseNotificationServiceError):
    """Raise it if template does not exist."""


class NotificationService:
    async def create_notification_by_template_id(
        self,
        db_session: AsyncSession,
        template_id: UUID4,
        contact: str,
        username: str,
        delivery_method: DeliveryMethodEnum,
    ) -> Notification:
        try:
            template = await Template.get_by_id(db_session, template_id)
        except ObjectDoesNotExistError:
            raise TemplateDoesNotExist

        return await self.create_notification(
            db_session, template, contact, username, delivery_method
        )

    async def create_notification_by_template_name(
        self,
        db_session: AsyncSession,
        template_name: str,
        contact: str,
        username: str,
        delivery_method: DeliveryMethodEnum,
    ) -> Notification:
        try:
            template = await Template.get_by_name(db_session, template_name)
        except ObjectDoesNotExistError:
            raise TemplateDoesNotExist

        return await self.create_notification(
            db_session, template, contact, username, delivery_method
        )

    @staticmethod
    async def create_notification(
        db_session: AsyncSession,
        template: Template,
        contact: str,
        username: str,
        delivery_method: DeliveryMethodEnum,
    ) -> Notification:
        notification = Notification(
            contact=contact,
            username=username,
            template_id=template.id,
            delivery_method=delivery_method,
        )
        db_session.add(notification)
        await db_session.flush([notification])
        await db_session.refresh(notification)

        return notification


def get_notification_service() -> NotificationService:
    return NotificationService()
