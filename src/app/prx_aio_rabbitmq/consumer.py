"""
This module provides facilities for consuming messages from RabbitMQ.
"""
import asyncio
import inspect
import logging
from typing import Callable, Any, Optional

from aio_pika import IncomingMessage

from app.prx_aio_rabbitmq.connection import Connection
from app.settings import settings

logger = logging.getLogger("prx-aio-rabbitmq")


class Consumer:
    """
    This class is intended to consume messages from RabbitMQ.
    """

    # pylint: disable=too-many-instance-attributes, disable=too-many-arguments
    def __init__(
        self,
        url: str = None,
        exchange_name: str = None,
        exchange_type: str = None,
        routing_key: str = None,
        callback: Callable = None,
        callback_data: Any = None,
        queue_name: str = None,
        exchange_durable: bool = None,
        exchange_auto_delete: bool = False,
        queue_durable: bool = False,
        queue_auto_delete: bool = True,
        queue_message_ttl: int = 0,
        connection: Optional[Connection] = None,
    ):
        """
        Creates a RabbitMQ consumer

        Args:
            url: an url like "amqp://guest:guest@127.0.0.1/"
            exchange_name: name of exchange
            exchange_type: type of exchange
            routing_key: routing key
            callback: callback function or async coroutine
                The callback function should support at least one argument of dict type,
                in which the incoming message will be passed.
                It can also support the second parameter for callback_data.
                Example:
                    def my_callback(message, data)
            callback_data: data, which will be passed to callback function
            queue_name: name of RabbitMQ queue. If None then queue with random name will be created
            exchange_durable: set exchange durable (default: False)
            exchange_auto_delete: set auto_delete property to exchange (default: True)
            queue_durable: set queue durable (default: False)
            queue_auto_delete: set queue auto_delete (default: True)
            queue_message_ttl: time to live for message in the queue (infinite if not set)
            connection: connection to RabbitMQ

        """
        assert exchange_name is not None
        assert exchange_type is not None
        assert routing_key is not None
        self.url = url
        self.exchange_name = exchange_name
        self.exchange_type = exchange_type
        self.routing_key = routing_key
        self.callback = callback
        self.callback_data = callback_data
        self.exchange_durable = exchange_durable
        self.exchange_auto_delete = exchange_auto_delete
        self.queue_durable = queue_durable
        self.queue_auto_delete = queue_auto_delete
        self.queue_message_ttl = queue_message_ttl
        self.connection_owner = connection is None
        self.connection = connection
        self.channel = None
        self.exchange = None
        self.queue = None
        self.consumer_tag = None
        self.queue_name = queue_name

    async def connect(self, fail_on_error: bool = False):
        """
        Connects to RabbitMQ
        """
        await self._delayed_connect(settings.RMQ.FIRST_CONNECTION_DELAY, fail_on_error)

    async def _delayed_connect(self, delay: int, fail_on_error: bool):
        try:
            await asyncio.sleep(delay)
            logger.debug("Consumer %s: try connect", self.exchange_name)
            await self._connect()
            logger.debug("Consumer %s: connected", self.exchange_name)
        except ConnectionError:
            logger.exception("Consumer %s: connection error", self.exchange_name)
            if fail_on_error:
                raise
            asyncio.ensure_future(
                self._delayed_connect(
                    settings.RMQ.RECONNECTION_DELAY, fail_on_error=False
                )
            )

    async def _connect(self) -> None:
        """
        Connects to RabbitMQ
        """
        if self.connection_owner:
            if self.connection is None:
                self.connection = Connection(url=self.url)
            await self.connection.connect()
        elif not self.connection.connected:
            raise ConnectionError("Connection to RabbitMQ not established")

        self.channel = await self.connection.c.channel()
        self.exchange = await self.channel.declare_exchange(
            self.exchange_name,
            self.exchange_type,
            durable=self.exchange_durable,
            auto_delete=self.exchange_auto_delete,
        )

        queue_arguments = None
        if self.queue_message_ttl > 0:
            queue_arguments = {"x-message-ttl": self.queue_message_ttl}

        self.queue = await self.channel.declare_queue(
            name=self.queue_name,
            auto_delete=self.queue_auto_delete,
            durable=self.queue_durable,
            arguments=queue_arguments,
        )
        await self.queue.bind(self.exchange, routing_key=self.routing_key)
        self.consumer_tag = await self.queue.consume(self._on_consume_message)

    async def disconnect(self) -> None:
        """
        Disconnects from RabbitMQ
        """
        await self.queue.cancel(self.consumer_tag)
        await self.queue.unbind(
            self.exchange, routing_key=self.routing_key, timeout=0.1
        )
        if self.queue_name is None:
            await self.queue.delete()
        await self.channel.close()
        if self.connection_owner:
            await self.connection.disconnect()

    async def _on_consume_message(self, incoming_message: IncomingMessage) -> None:
        try:
            if self.callback is not None:
                message_body = incoming_message.body.decode()
                logger.debug(
                    "Consumer %s received: %s", self.exchange_name, message_body
                )

                message_dict = {**incoming_message.info(), "body": message_body}
                if inspect.iscoroutinefunction(self.callback):
                    if self.callback_data:
                        await self.callback(message_dict, self.callback_data)
                    else:
                        await self.callback(message_dict)
                else:
                    if self.callback_data:
                        self.callback(message_dict, self.callback_data)
                    else:
                        self.callback(message_dict)
            incoming_message.ack()
        except Exception:  # pylint: disable = broad-except
            logger.exception("Error in callback")
            incoming_message.nack()
