"""
This module provides facilities for publishing messages to RabbitMQ queue.
"""
import asyncio
import logging
from contextlib import suppress
from datetime import datetime, timedelta
from typing import Optional

from aio_pika import Message, DeliveryMode

from app.prx_aio_rabbitmq.connection import Connection
from app.settings import settings

logger = logging.getLogger("prx-aio-rabbitmq")


class Publisher:
    """
    This class is intended for publishing messages to RabbitMQ exchange.
    """

    def __init__(
        self,
        url: str = None,
        exchange_name: str = None,
        exchange_type: str = None,
        publish_timeout: float = 0,
        exchange_durable: bool = None,
        exchange_auto_delete: bool = False,
        connection: Optional[Connection] = None,
    ):
        """
        Publisher for RabbitMQ

        Args:
            url: an url like "amqp://guest:guest@127.0.0.1/"
            exchange_name: name of exchange
            exchange_type: type of exchange
            publish_timeout: publish timeout in seconds (0 - infinite)
            exchange_durable: set exchange durable (default: False)
            exchange_auto_delete: set auto_delete property to exchange (default: True)
            connection: connection to RabbitMQ

        """
        assert exchange_name is not None
        assert exchange_type is not None
        self.url = url
        self.exchange_name = exchange_name
        self.exchange_type = exchange_type
        self.publish_timeout = publish_timeout
        self.exchange_durable = exchange_durable
        self.exchange_auto_delete = exchange_auto_delete
        self.connection_owner = connection is None
        self.connection = connection
        self.channel = None
        self.exchange = None
        self._queue = asyncio.Queue()
        self._task = None

    async def connect(self, fail_on_error: bool = False) -> None:
        """
        Connect to RabbitMQ
        """
        await self._delayed_connect(settings.RMQ.FIRST_CONNECTION_DELAY, fail_on_error)
        self._task = asyncio.ensure_future(self._run())

    async def _delayed_connect(self, delay: int, fail_on_error: bool):
        if delay > 0:
            await asyncio.sleep(delay)
        try:
            logger.debug("Publisher %s: try to connect", self.exchange_name)
            await self._connect()
            logger.debug("Publisher %s: connected", self.exchange_name)
        except ConnectionError:
            logger.exception("Publisher %s: connection error", self.exchange_name)
            if fail_on_error:
                raise
            asyncio.ensure_future(
                self._delayed_connect(settings.RMQ.RESEND_DELAY, fail_on_error=False)
            )

    async def _connect(self) -> None:
        """
        Perform connect to RabbitMQ
        """
        if self.connection_owner:
            if self.connection is None:
                self.connection = Connection(url=self.url)
            await self.connection.connect()
        elif not self.connection.connected:
            raise ConnectionError("RabbitMQ connection not established")

        self.channel = await self.connection.c.channel()
        self.exchange = await self.channel.declare_exchange(
            self.exchange_name,
            self.exchange_type,
            durable=self.exchange_durable,
            auto_delete=self.exchange_auto_delete,
        )

    async def disconnect(self) -> None:
        """
        Disconnects from RabbitMQ
        """
        logger.debug("Publisher %s: disconnect", self.exchange_name)
        await self._queue.put("")
        self._task.cancel()
        await self.channel.close()
        if self.connection_owner:
            await self.connection.disconnect()

    async def _do_publish(self, routing_key: str, message: Message):
        await self.exchange.publish(
            message,
            routing_key=routing_key,
        )

    async def _run(self):
        logger.debug("Publisher %s: task started", self.exchange_name)
        try:
            with suppress(asyncio.CancelledError):
                while True:
                    item = await self._queue.get()  # type: tuple
                    if item is None:
                        return

                    routing_key, message, publish_deadline = item
                    try:
                        logger.debug(
                            "Publisher %s: publish %s", self.exchange_name, message.body
                        )
                        await self._do_publish(routing_key, message)
                        logger.debug("Publisher %s: publish OK", self.exchange_name)
                    except Exception:  # pylint: disable = broad-except
                        logger.exception(
                            "Publisher %s: error send message", self.exchange_name
                        )
                        if (
                            publish_deadline is None
                            or publish_deadline > datetime.now()
                        ):
                            await self._queue.put(item)
                        await asyncio.sleep(settings.RMQ.RESEND_DELAY)
        except Exception:  # pylint: disable = broad-except
            logger.exception("Publisher %s: task started", self.exchange_name)
        finally:
            logger.debug("Publisher %s: task finished", self.exchange_name)

    async def publish(
        self,
        routing_key: str,
        message_body: str,
        correlation_id: str = None,
        persistent: bool = False,
    ) -> None:
        """
        Publish message to RabbitMQ

        Args:
            routing_key: routing key
            message_body: message
            correlation_id: correlation_id if needed. Default value is None.
            persistent: flag message to keep it on hard disk by RabbitMQ

        """
        logger.debug("Publisher %s: enqueue %s", self.exchange_name, message_body)
        message = Message(
            message_body.encode(),
            correlation_id=correlation_id,
            delivery_mode=DeliveryMode.PERSISTENT
            if persistent
            else DeliveryMode.NOT_PERSISTENT,
        )

        publish_deadline = None
        if self.publish_timeout > 0:
            publish_deadline = datetime.now() + timedelta(seconds=self.publish_timeout)
        await self._queue.put((routing_key, message, publish_deadline))
