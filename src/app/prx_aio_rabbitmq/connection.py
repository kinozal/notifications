"""
Module with connection wrapper to RabbitMQ
"""
import asyncio
import logging

import aio_pika
from aio_pika.connection import ConnectionType

from app.settings import settings

logger = logging.getLogger("prx-aio-rabbitmq")


class Connection:
    """
    Performs connect to RabbitMQ and disconnect
    """

    def __init__(
        self,
        url: str = None,
        host: str = "localhost",
        port: int = 5672,
        login: str = None,
        password: str = None,
    ):
        self.url = url
        self.host = host
        self.port = port
        self.login = login
        self.password = password
        self.connected = False
        self.connection = None

    @property
    def c(self) -> ConnectionType:
        """
        Property for shorten access to connection object from aio_pika
        """
        return self.connection

    async def connect(self, fail_on_error: bool = False) -> None:
        """
        Connect to RabbitMQ
        """
        await self._delayed_connect(settings.RMQ.FIRST_CONNECTION_DELAY, fail_on_error)

    async def _delayed_connect(self, delay: int, fail_on_error: bool):
        if delay > 0:
            await asyncio.sleep(delay)
        try:
            logger.debug("Try to connect to RabbitMQ")
            await self._connect()
            logger.debug("Connected to RabbitMQ")
        except ConnectionError:
            logger.exception("Cannot connect to RabbitMQ")
            if fail_on_error:
                raise
            asyncio.ensure_future(
                self._delayed_connect(
                    settings.RMQ.RECONNECTION_DELAY, fail_on_error=False
                )
            )

    async def _connect(self) -> None:
        """
        Perform connect to RabbitMQ
        """
        self.connection = await aio_pika.connect_robust(
            url=self.url,
            host=self.host,
            port=self.port,
            login=self.login,
            password=self.password,
            loop=asyncio.get_event_loop(),
        )
        self.connected = True

    async def disconnect(self) -> None:
        """
        Disconnects from RabbitMQ
        """
        logger.debug("Disconnect from RabbitMQ")
        await self.connection.close()
