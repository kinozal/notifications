"""
Basic JSON-RPC exception are described in this module
"""


class JsonParseError(Exception):
    """
    JSON parse error
    """

    code = -32700


class InvalidRequest(Exception):
    """
    JSON-RPC request is invalid
    """

    code = -32600


class MethodNotFound(Exception):
    """
    Handler for method not found
    """

    code = -32601


class InvalidMethodParameters(Exception):
    """
    Parameters in request are not compartible with method
    """

    code = -32602


class InternalError(Exception):
    """
    Unexpected internal server error
    """

    code = -32603


class TimeoutException(Exception):
    """
    Server did not answer in specified timeout
    """

    code = -32200
