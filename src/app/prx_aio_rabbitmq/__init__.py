"""
RabbitMQ library

"""

__version__ = "1.0.0"
__author__ = "Practix Middle python Cohort#14 Team#2"
__maintainer__ = __author__

__email__ = "inbox@inbox.ru"

__all__ = ("__author__", "__email__", "__maintainer__")
