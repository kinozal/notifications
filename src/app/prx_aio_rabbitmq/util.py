"""
Utility functions
"""

import aio_pika


async def delete_exchange(connection_url: str, exchange_name: str):
    """
    Deletes exchange from RabbitMQ

    Args:
        connection_url:
        exchange_name:

    """
    connection = await aio_pika.connect(connection_url)
    channel = await connection.channel()
    await channel.exchange_delete(exchange_name)
    await channel.close()
    await connection.close()
