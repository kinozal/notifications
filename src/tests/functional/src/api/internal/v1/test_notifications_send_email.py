from typing import Any
from unittest.mock import ANY, MagicMock

import orjson
import pytest
from async_asgi_testclient import TestClient
from pytest_mock import MockerFixture
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.amqp import publisher, QueueNameEnum
from app.models import Template, Notification
from tests.functional.utils import fake

FROZEN_TIME = "2022-07-22T17:00:00"
pytestmark = [pytest.mark.asyncio, pytest.mark.freeze_time(FROZEN_TIME)]

PATH = "/api/internal/v1/notifications/send-email"


@pytest.fixture
async def template(request, db_session: AsyncSession) -> Template:
    data = """
    <!DOCTYPE html>
    <html>
    <body>
    </body>
    </html>
    """
    template = Template(data=data, name=request.param)
    db_session.add(template)
    await db_session.commit()

    yield template


@pytest.fixture
async def request_data(request) -> dict[str, Any]:
    return {
        "contact": fake.person.email(),
        "username": fake.person.username(),
        "template_name": request.param,
    }


@pytest.fixture
async def expected_response(request_data: dict[str, Any]) -> dict[str, Any]:
    return {
        "id": ANY,
        "created_at": FROZEN_TIME,
        "updated_at": FROZEN_TIME,
        "template_id": ANY,
        "contact": request_data["contact"],
        "username": request_data["username"],
        "status": "CREATED",
        "delivery_method": "EMAIL",
    }


@pytest.fixture
async def mocked_amqp_publisher(mocker: MockerFixture) -> MagicMock:
    return mocker.patch.object(publisher, "publish", return_value=None)


@pytest.mark.parametrize(
    "template,request_data,expected_status",
    [
        ("expected", "expected", 200),
    ],
    indirect=["template", "request_data"],
)
async def test_ok(
    db_session: AsyncSession,
    client: TestClient,
    request_data: dict[str, Any],
    template: Template,
    expected_status: int,
    expected_response: dict[str, Any],
    mocked_amqp_publisher: MagicMock,
):
    response = await client.post(PATH, json=request_data)

    assert response.status_code == expected_status, response.text
    assert response.json() == expected_response

    stmt = select(Notification).where(Notification.contact == request_data["contact"])
    db_result = await db_session.execute(stmt)
    notification = db_result.scalar()

    expected_msg = {
        "data": str(notification.id),
        "x_request_id": client.headers["x-request-id"],
    }

    mocked_amqp_publisher.assert_called_with(
        routing_key=QueueNameEnum.email.value,
        message_body=orjson.dumps(expected_msg).decode(),
        persistent=True,
    )


@pytest.mark.parametrize(
    "template,request_data,expected_status",
    [
        ("unexpected", "expected", 404),
    ],
    indirect=["template", "request_data"],
)
async def test_template_not_found(
    client: TestClient,
    request_data: dict[str, Any],
    template: Template,
    expected_status: int,
):
    response = await client.post(PATH, json=request_data)

    assert response.status_code == expected_status, response.text
    assert response.json()["detail"] == "Template not found."
