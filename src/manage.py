import asyncio
from functools import wraps
from typing import Callable, Any

import typer
import uvicorn
from IPython import embed

from app.settings import settings

typer_app = typer.Typer()


def coro(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        return asyncio.run(func(*args, **kwargs))

    return wrapper


@typer_app.command()
def shell():
    embed()


@typer_app.command()
def runserver():
    uvicorn.run(**settings.UVICORN.dict())


@typer_app.command()
def run_email_worker():
    from app.tasks.background.events.worker import Worker
    from app.integrations.email import EmailHttpClient
    from app.transports import AiohttpTransport

    loop = asyncio.get_event_loop()

    worker = Worker(EmailHttpClient(AiohttpTransport()))
    loop.run_until_complete(worker.startup())
    loop.create_task(worker.run())

    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(worker.shutdown())


if __name__ == "__main__":
    typer_app()
