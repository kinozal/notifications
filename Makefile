start:
	docker-compose up --build -d

stop:
	docker-compose down

start-api:
	docker-compose up --build -d app

start-e-worker:
	docker-compose up --build -d email-worker

start-e-cronjob:
	docker-compose up --build -d email-cron-job

stop-api:
	docker-compose stop app

stop-e-worker:
	docker-compose stop email-worker

stop-e-cronjob:
	docker-compose stop email-cron-job

tests:
	docker-compose -f docker-compose.local.yml up --build --exit-code-from sut

migrate:
	docker-compose up -d app && docker-compose exec app alembic revision --autogenerate -m "$(name)"

db-shell:
	docker-compose up -d db && docker-compose exec db psql -U postgres -W notifications